Spree::Taxonomy.class_eval do
  def self.find_categories!
    find_by!(name: 'Categories')
  end
end
