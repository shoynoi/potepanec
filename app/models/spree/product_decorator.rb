Spree::Product.class_eval do
  NEW_PRODUCTS_PERIOD = 1.month.ago..Time.zone.now
  scope :with_price_images, -> { includes(master: [:default_price, :images]) }
  scope :take_random, ->(n) { where(id: ids.sample(n)) }
  scope :related_products, ->(product) {
    joins(:taxons).
      where(spree_taxons: { id: product.taxon_ids, taxonomy_id: Spree::Taxonomy.find_categories! }).
      with_price_images.
      where.not(id: product.id).
      distinct
  }
  scope :new_products, -> {
    where(available_on: NEW_PRODUCTS_PERIOD).
      with_price_images.
      order(available_on: :desc)
  }
end
