class Potepan::HomeController < ApplicationController
  MAX_FEATURED_PRODUCTS_COUNT = 8
  def index
    @featured_products = Spree::Product.new_products.first(MAX_FEATURED_PRODUCTS_COUNT)
  end
end
