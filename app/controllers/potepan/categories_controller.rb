class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Product.with_price_images.in_taxon(@taxon)
    @taxonomies = Spree::Taxonomy.includes(root: :children)
  end
end
