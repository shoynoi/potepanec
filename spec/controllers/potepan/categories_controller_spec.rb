require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:taxonomies) { Spree::Taxonomy.includes(root: :children) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "returns a 200 response" do
      expect(response).to have_http_status "200"
    end
    it "renders the show template" do
      expect(response).to render_template :show
    end
    it "assings the requested taxon" do
      expect(assigns(:taxon)).to eq taxon
    end
    it "assings the requested taxonomy" do
      expect(assigns(:taxonomies)).to eq taxonomies
    end
    it "assings the requested products" do
      create_list(:product, 2) do |product|
        taxon.products << product
      end
      expect(assigns(:products)).to eq(taxon.products)
    end
  end
end
