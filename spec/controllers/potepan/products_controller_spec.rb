require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let!(:taxonomy) { create(:taxonomy, name: "Categories") }
  let!(:taxon) { taxonomy.root.children.create(name: "Tops", taxonomy_id: taxonomy.id) }
  let!(:product) { create(:product, name: "Ruby T-Shirt") }

  describe "GET #show" do
    before do
      get :show, params: { id: product.id }
    end

    it "responds successfully" do
      expect(response).to be_success
    end
    it "returns a 200 response" do
      expect(response).to have_http_status "200"
    end
    it "assigns the requested product" do
      expect(assigns(:product)).to eq product
    end
  end

  describe "related products" do
    let!(:other_taxon) { taxonomy.root.children.create(name: "Knit", taxonomy_id: taxonomy.id) }
    let!(:other_product) { create(:product, name: "Ruby Knit") }

    before do
      product.taxons << taxon
      other_product.taxons << other_taxon
    end

    context "when a product has many taxons" do
      before do
        product.taxons << other_taxon
        other_product.taxons << taxon
        get :show, params: { id: product.id }
      end

      it "doesn't display the same related product" do
        expect(assigns(:related_products).size).to eq 1
      end
    end

    context "when many taxons exists" do
      before do
        create_list(:product, 3) do |related_product|
          related_product.taxons << taxon
        end
        get :show, params: { id: product.id }
      end

      it "display products of the same taxon" do
        expect(assigns(:related_products)).not_to include(other_product)
      end
    end

    context "when many taxonomies exist" do
      let(:other_taxonomy) { create(:taxonomy, name: "Brand") }
      let(:taxon_brand) { other_taxonomy.root.children.create(name: "Ruby", taxonomy_id: other_taxonomy.id) }

      before do
        product.taxons << taxon_brand
        other_product.taxons << taxon_brand
        get :show, params: { id: product.id }
      end

      it "displays only Categories taxonomy" do
        expect(assigns(:related_products)).not_to include(other_product)
      end
    end

    context "when 3 related_products" do
      before do
        create_list(:product, 3) do |related_product|
          related_product.taxons << taxon
        end
        get :show, params: { id: product.id }
      end

      it "displays 3 related products" do
        expect(assigns(:related_products).size).to eq 3
      end
    end

    context "when 5 related_products" do
      before do
        create_list(:product, 5) do |related_product|
          related_product.taxons << taxon
        end
        get :show, params: { id: product.id }
      end

      it "displays maximum 4 related products" do
        expect(assigns(:related_products).size).to eq 4
      end
    end
  end
end
