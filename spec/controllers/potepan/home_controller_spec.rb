require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "featured_products" do
    let!(:new_products) do
      [
        create(:product, available_on: 1.day.ago),
        create(:product, available_on: 2.day.ago),
        create(:product, available_on: 3.day.ago),
        create(:product, available_on: 1.month.ago + 1.day),
      ]
    end
    let!(:old_product) { create(:product, available_on: 1.month.ago - 1.day) }
    let!(:future_product) { create(:product, available_on: 1.day.from_now) }

    before do
      get :index
    end

    it "has products within 1 month ago" do
      expect(assigns(:featured_products)).to include(*new_products)
    end
    it "doesn't have products over 1 month ago" do
      expect(assigns(:featured_products)).not_to include(old_product)
    end
    it "doesn't have products available_on date is in the future" do
      expect(assigns(:featured_products)).not_to include(future_product)
    end
    it "has products that sort available_on in descending order" do
      expect(assigns(:featured_products)).to match(new_products)
    end

    context "when count of featured_products is more than 8" do
      let!(:new_products) { create_list(:product, 9, available_on: 1.week.ago) }

      it "has maximum 8 products" do
        get :index
        expect(assigns(:featured_products).size).to eq 8
      end
    end
  end
end
