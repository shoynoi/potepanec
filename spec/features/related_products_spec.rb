require 'rails_helper'

RSpec.feature "RelatedProducts", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: 'Categories') }
  let(:taxon) { taxonomy.root.children.create(name: 'Clothing', taxonomy_id: taxonomy.id) }
  let(:t_shirt) { create(:product, name: 'Ruby T-Shirt') }
  let(:jacket) { create(:product, name: 'Apache Jacket') }

  before do
    taxon.products << t_shirt
    taxon.products << jacket
  end

  scenario "show related_products" do
    visit potepan_product_path(t_shirt.id)
    expect(page).to have_content('Ruby T-Shirt')
    expect(page).to have_link('Apache Jacket')
    click_on 'Apache Jacket'
    expect(current_path).to eq potepan_product_path(jacket.id)
    expect(page).to have_content('Apache Jacket')
    expect(page).to have_link('Ruby T-Shirt')
  end
end
