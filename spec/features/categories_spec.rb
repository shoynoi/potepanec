require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: 'Categories') }
  let(:clothing) { taxonomy.root.children.create(name: 'Clothing') }
  let(:bags) { taxonomy.root.children.create(name: 'Bags') }
  let(:t_shirt) { create(:product, name: 'Ruby T-Shirt') }
  let(:jacket) { create(:product, name: 'Apache Jacket') }
  let(:tote_bag) { create(:product, name: 'Ruby Tote Bag') }

  before do
    clothing.products << t_shirt
    clothing.products << jacket
    bags.products << tote_bag
  end

  scenario 'products of the category are displayed' do
    visit potepan_category_path(clothing.id)
    expect(page).to have_content('Ruby T-Shirt')
    expect(page).to have_content('Apache Jacket')
    expect(page).to have_no_content('Ruby Tote Bag')
  end

  scenario 'link to product detail page' do
    visit potepan_category_path(clothing.id)
    expect(page).to have_link('Ruby T-Shirt')
    click_on 'Ruby T-Shirt'
    expect(current_path).to eq potepan_product_path(t_shirt.id)
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(clothing.id)
  end

  scenario 'link taxons' do
    visit potepan_category_path(clothing.id)
    click_on 'Bags'
    expect(current_path).to eq potepan_category_path(bags.id)
  end
end
