require 'rails_helper'

RSpec.feature "FeaturedProducts", type: :feature do
  let!(:taxonomy) { create(:taxonomy, name: "Categories") }
  let!(:taxon) { taxonomy.root.children.create(name: "T-Shirts", taxonomy_id: taxonomy.id) }
  let!(:new_product) { create(:product, name: 'New T-Shirt', available_on: 1.day.ago) }
  let!(:old_product) { create(:product, name: 'Old T-Shirt', available_on: 2.month.ago) }
  let!(:future_product) { create(:product, name: 'Future T-Shirt', available_on: 1.day.from_now) }

  before do
    taxon.products += [new_product, old_product, future_product]
  end

  scenario 'show featured_products' do
    visit potepan_root_path
    expect(page).to have_link('New T-Shirt')
    expect(page).to have_no_content('Old T-Shirt')
    expect(page).to have_no_content('Future T-Shirt')
    click_on 'New T-Shirt'
    expect(current_path).to eq potepan_product_path(new_product.id)
  end
end
