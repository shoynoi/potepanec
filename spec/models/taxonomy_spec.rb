require 'rails_helper'

RSpec.describe Spree::Taxonomy, type: :model do
  describe "#self.find_categories!" do
    context "when not exist categories in taxonomy" do
      it "raises error" do
        create(:taxonomy)
        expect { Spree::Taxonomy.find_categories! }.to raise_error ActiveRecord::RecordNotFound
      end
    end

    context "when exist categories in taxonomy" do
      let!(:taxonomy) { create(:taxonomy, name: "Categories") }

      it "finds categories" do
        expect(Spree::Taxonomy.find_categories!).to eq taxonomy
      end
    end
  end
end
